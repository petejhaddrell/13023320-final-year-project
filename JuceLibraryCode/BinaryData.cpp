/* ==================================== JUCER_BINARY_RESOURCE ====================================

   This is an auto-generated file: Any edits you make may be overwritten!

*/

namespace BinaryData
{

//================== Launch Screen.storyboard ==================
static const unsigned char temp_binary_data_0[] =
"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\r\n"
"<document type=\"com.apple.InterfaceBuilder3.CocoaTouch.Storyboard.XIB\" version=\"3.0\" toolsVersion=\"9532\" systemVersion=\"14F1021\" targetRuntime=\"iOS.CocoaTouch\" propertyAccessControl=\"none\" useAutolayout=\"YES\" launchScreen=\"YES\" useTra"
"itCollections=\"YES\" initialViewController=\"01J-lp-oVM\">\r\n"
"    <dependencies>\r\n"
"        <plugIn identifier=\"com.apple.InterfaceBuilder.IBCocoaTouchPlugin\" version=\"9530\"/>\r\n"
"        <capability name=\"Constraints with non-1.0 multipliers\" minToolsVersion=\"5.1\"/>\r\n"
"    </dependencies>\r\n"
"    <scenes>\r\n"
"        <!--View Controller-->\r\n"
"        <scene sceneID=\"EHf-IW-A2E\">\r\n"
"            <objects>\r\n"
"                <viewController id=\"01J-lp-oVM\" sceneMemberID=\"viewController\">\r\n"
"                    <layoutGuides>\r\n"
"                        <viewControllerLayoutGuide type=\"top\" id=\"Llm-lL-Icb\"/>\r\n"
"                        <viewControllerLayoutGuide type=\"bottom\" id=\"xb3-aO-Qok\"/>\r\n"
"                    </layoutGuides>\r\n"
"                    <view key=\"view\" contentMode=\"scaleToFill\" id=\"Ze5-6b-2t3\">\r\n"
"                        <rect key=\"frame\" x=\"0.0\" y=\"0.0\" width=\"600\" height=\"600\"/>\r\n"
"                        <autoresizingMask key=\"autoresizingMask\" widthSizable=\"YES\" heightSizable=\"YES\"/>\r\n"
"                        <subviews>\r\n"
"                            <label opaque=\"NO\" clipsSubviews=\"YES\" userInteractionEnabled=\"NO\" contentMode=\"left\" horizontalHuggingPriority=\"251\" verticalHuggingPriority=\"251\" misplaced=\"YES\" text=\"\" textAlignment=\"center\" lineBreak"
"Mode=\"tailTruncation\" baselineAdjustment=\"alignBaselines\" minimumFontSize=\"9\" translatesAutoresizingMaskIntoConstraints=\"NO\" id=\"obG-Y5-kRd\">\r\n"
"                                <rect key=\"frame\" x=\"20\" y=\"559\" width=\"560\" height=\"21\"/>\r\n"
"                                <fontDescription key=\"fontDescription\" type=\"system\" pointSize=\"17\"/>\r\n"
"                                <color key=\"textColor\" red=\"0.0\" green=\"0.0\" blue=\"0.0\" alpha=\"1\" colorSpace=\"calibratedRGB\"/>\r\n"
"                                <nil key=\"highlightedColor\"/>\r\n"
"                            </label>\r\n"
"                            <label opaque=\"NO\" clipsSubviews=\"YES\" userInteractionEnabled=\"NO\" contentMode=\"left\" horizontalHuggingPriority=\"251\" verticalHuggingPriority=\"251\" text=\"13023320-FYP\" textAlignment=\"center\" lineBreakMode=\""
"middleTruncation\" baselineAdjustment=\"alignBaselines\" minimumFontSize=\"18\" translatesAutoresizingMaskIntoConstraints=\"NO\" id=\"GJd-Yh-RWb\">\r\n"
"                                <rect key=\"frame\" x=\"20\" y=\"180\" width=\"560\" height=\"43\"/>\r\n"
"                                <fontDescription key=\"fontDescription\" type=\"boldSystem\" pointSize=\"36\"/>\r\n"
"                                <color key=\"textColor\" red=\"0.0\" green=\"0.0\" blue=\"0.0\" alpha=\"1\" colorSpace=\"calibratedRGB\"/>\r\n"
"                                <nil key=\"highlightedColor\"/>\r\n"
"                            </label>\r\n"
"                        </subviews>\r\n"
"                        <color key=\"backgroundColor\" white=\"1\" alpha=\"1\" colorSpace=\"custom\" customColorSpace=\"calibratedWhite\"/>\r\n"
"                        <constraints>\r\n"
"                            <constraint firstAttribute=\"centerX\" secondItem=\"obG-Y5-kRd\" secondAttribute=\"centerX\" id=\"5cz-MP-9tL\"/>\r\n"
"                            <constraint firstAttribute=\"centerX\" secondItem=\"GJd-Yh-RWb\" secondAttribute=\"centerX\" id=\"Q3B-4B-g5h\"/>\r\n"
"                            <constraint firstItem=\"obG-Y5-kRd\" firstAttribute=\"leading\" secondItem=\"Ze5-6b-2t3\" secondAttribute=\"leading\" constant=\"20\" symbolic=\"YES\" id=\"SfN-ll-jLj\"/>\r\n"
"                            <constraint firstAttribute=\"bottom\" secondItem=\"obG-Y5-kRd\" secondAttribute=\"bottom\" constant=\"20\" id=\"Y44-ml-fuU\"/>\r\n"
"                            <constraint firstItem=\"GJd-Yh-RWb\" firstAttribute=\"centerY\" secondItem=\"Ze5-6b-2t3\" secondAttribute=\"bottom\" multiplier=\"1/3\" constant=\"1\" id=\"moa-c2-u7t\"/>\r\n"
"                            <constraint firstItem=\"GJd-Yh-RWb\" firstAttribute=\"leading\" secondItem=\"Ze5-6b-2t3\" secondAttribute=\"leading\" constant=\"20\" symbolic=\"YES\" id=\"x7j-FC-K8j\"/>\r\n"
"                        </constraints>\r\n"
"                    </view>\r\n"
"                </viewController>\r\n"
"                <placeholder placeholderIdentifier=\"IBFirstResponder\" id=\"iYj-Kq-Ea1\" userLabel=\"First Responder\" sceneMemberID=\"firstResponder\"/>\r\n"
"            </objects>\r\n"
"            <point key=\"canvasLocation\" x=\"53\" y=\"375\"/>\r\n"
"        </scene>\r\n"
"    </scenes>\r\n"
"</document>\r\n";

const char* Launch_Screen_storyboard = (const char*) temp_binary_data_0;


const char* getNamedResource (const char*, int&) throw();
const char* getNamedResource (const char* resourceNameUTF8, int& numBytes) throw()
{
    unsigned int hash = 0;
    if (resourceNameUTF8 != 0)
        while (*resourceNameUTF8 != 0)
            hash = 31 * hash + (unsigned int) *resourceNameUTF8++;

    switch (hash)
    {
        case 0x69dcab38:  numBytes = 4466; return Launch_Screen_storyboard;
        default: break;
    }

    numBytes = 0;
    return 0;
}

const char* namedResourceList[] =
{
    "Launch_Screen_storyboard"
};

}
