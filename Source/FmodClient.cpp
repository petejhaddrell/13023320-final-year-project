//
//  FmodClient.cpp
//  13023320-FYP
//
//  Created by Pete Haddrell on 09/03/2016.
//
//

#include "../JuceLibraryCode/JuceHeader.h"
#include "FmodClient.h"
#include "math.h"

// an error check applied to all FMOD implementations
static void ERRCHECK (FMOD_RESULT result)
{
    if (result != FMOD_OK)
    {
        jassert (false);
    }
}


// function to test whether an event is live
bool eventIsLive (EventInstance* instance)
{
    FMOD_STUDIO_PLAYBACK_STATE state;
    FMOD_RESULT error = instance->getPlaybackState (&state);
    return (error == FMOD_OK) && (state != FMOD_STUDIO_PLAYBACK_STOPPED);
}


FmodClient::FmodClient()
:   system   (nullptr),
    lowlevel (nullptr),
    instance (nullptr)
{
    outputTrack = 0.0;
    outputCount = 0;
}


FmodClient::~FmodClient()
{
    shutdown();
}


void FmodClient::init (File resources)
{
    ERRCHECK (FMOD::Studio::System::create (&system));
    ERRCHECK (system->getLowLevelSystem (&lowlevel));
    
    // sets the music to 44.1 sample rate and stereo
    ERRCHECK (lowlevel->setSoftwareFormat (44100, FMOD_SPEAKERMODE_STEREO, 0));
    ERRCHECK (system->initialize (32,
                                  FMOD_STUDIO_INIT_LIVEUPDATE | FMOD_STUDIO_INIT_ALLOW_MISSING_PLUGINS,
                                  FMOD_INIT_PROFILE_ENABLE,
                                  0));
    
    // gets the bank files from the iphone system
    ERRCHECK (system->loadBankFile (resources.getChildFile ("Master Bank.bank").getFullPathName().toUTF8(), FMOD_STUDIO_LOAD_BANK_NORMAL, &bank));
    ERRCHECK (system->loadBankFile (resources.getChildFile ("Master Bank.strings.bank").getFullPathName().toUTF8(), FMOD_STUDIO_LOAD_BANK_NORMAL, &stringsBank));
}


void FmodClient::shutdown()
{
    if (bank != nullptr)
    {
        ERRCHECK (bank->unload());
    }
    
    if (stringsBank != nullptr)
    {
        ERRCHECK (stringsBank->unload());
    }
    
    if (system != nullptr)
    {
        if (system->isValid())
        {
            ERRCHECK (system->release());
        }
    }
    
    system = nullptr;
    lowlevel = nullptr;
}


String FmodClient::setEventPath (float output0)
{
    float roundingVal = remainderf (output0, 0.05);
    output0 -= roundingVal;
    
    // if the music has played previously it changes the selected song
    if (outputTrack == output0)
    {
        if (outputCount == 0)
        {
            if (output0 >= 1.0)
            {
                output0 -= 0.05;
            }
            
            else
            {
                output0 += 0.05;
            }
            outputCount = 1;
        }
        else if (outputCount == 1)
        {
            if (output0 <= 0.0)
            {
                output0 += 0.10;
            }
            
            else
            {
                output0 -= 0.10;
            }
            
            outputCount = 2;
        }
        else if (outputCount == 2)
        {
            if (output0 >= 1.0)
            {
                output0 -= 0.05;
            }
            
            else
            {
                output0 += 0.05;
            }
            outputCount = 0;
        }
        
    }
    
    // builds a string for the event file path
    String outputVal (output0, 2);
    String eventPath = "event:/";
    eventPath += outputVal;
    
    outputTrack = output0;
    return eventPath;
}


void FmodClient::setEvent (float output0, float output1, float output2)
{
    // does not change the song unless the music event is not live
    if (! eventIsLive (instance))
    {
        EventDescription* desc = nullptr;
        
        // sets up the FMOD instance
        ERRCHECK (system->getEvent ((setEventPath (output0)).toUTF8() , &desc));
        ERRCHECK (desc->createInstance (&instance));
        ERRCHECK (instance->getParameter ("mixer", &mixer));
        ERRCHECK (mixer->setValue (output1));
        
        ERRCHECK (instance->getParameter ("reverb", &reverb));
        ERRCHECK (reverb->setValue (output2));
        ERRCHECK (instance->start());
        update();
    }
    
    else
        return;
}


void FmodClient::update()
{
    if (system == nullptr)
        return;
    
    if (! system->isValid())
        return;
    
    ERRCHECK (system->update());
}


void FmodClient::eventStart()
{
    if (instance != nullptr)
    {
        // checks if the event is live, if true it unpauses it
        if (eventIsLive (instance))
        {
            ERRCHECK(instance->setPaused (false));
        }
        else
        {
            ERRCHECK (instance->start());
        }
        
        update();
    }
}


void FmodClient::eventPause()
{
    if (instance != nullptr)
    {
        ERRCHECK (instance->setPaused (true));
        update();
    }
}


void FmodClient::eventStop()
{
    if (instance != nullptr)
    {
        ERRCHECK(instance->stop (FMOD_STUDIO_STOP_ALLOWFADEOUT));
        update();
    }
}