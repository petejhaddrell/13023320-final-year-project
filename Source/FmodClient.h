//
//  FmodClient.h
//  13023320-FYP
//
//  Created by Pete Haddrell on 09/03/2016.
//
//

#ifndef FmodClient_h
#define FmodClient_h

#include "fmod_errors.h"
#include "fmod_studio.hpp"
#include "fmod.hpp"
#include "JuceHeader.h"

using namespace FMOD::Studio;

/** A class that handles all FMOD related code */
class FmodClient
{
public:
    /** Constructor */
    FmodClient();
    
    /** Destructor */
    ~FmodClient();
    
    /** Setup process for FMOD */
    void init (File resources);
    
    /** Shutdown procedure for FMOD */
    void shutdown();
    
    /** Builds an eventPath string for the description */
    String setEventPath (float output0);
    
    /** Sets and starts the FMOD event from the output nodes */
    void setEvent (float output0, float output1, float output2);
    
    /** Updates the FMOD system */
    void update();
    
    /** Controls the start function of the event */
    void eventStart();
    
    /** Controls the pause function of the event */
    void eventPause();
    
    /** Controls the stop function of the event */
    void eventStop();
    
private:
    System* system;             // gives the ability to retrieve the project
    FMOD::System* lowlevel;     // allows the lowlevel functionality to be moddified
    Bank* bank;                 // gets the bank from the FMOD project
    Bank* stringsBank;          // get the strings bank from the FMOD project
    EventInstance* instance;    // the instance of the event is utilised by this variable
    ParameterInstance* mixer;   // gives control of the mixer parameter, used by the weather class
    ParameterInstance* reverb;  // gives control of the reverb parameter, used by the crime class

    float outputTrack;          // tracks the output of the 0 node
    int outputCount;            // tracks the position within the randomisation functionality
};

#endif /* FmodClient_h */
