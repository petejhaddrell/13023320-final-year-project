#include "MainComponent.h"
#include "PageOne.h"
#include "PageTwo.h"

Component* createMainContentComponent()     { return new MainContentComponent(); }

MainContentComponent::MainContentComponent()
{    
    PageOne* pageOneComp = new PageOne();
    pageOneComp->setOwner (this);
    addAndMakeVisible (pageOneComp);
    pageOneComp->setTopLeftPosition (0, 0);
    
    neuralNetwork.setOwner(this);
    
    startTimer (1000);
    
    setSize (800, 600);
}

MainContentComponent::~MainContentComponent()
{
    stopTimer();
}

void MainContentComponent::paint (Graphics& g)
{
    g.fillAll (Colours::azure);
}

void MainContentComponent::resized()
{
    
}

void MainContentComponent::timerCallback()
{
    neuralNetwork.update();
}
