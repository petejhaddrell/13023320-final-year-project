//
//  MainComponent.h
//  13023320-FYP
//
//  Created by Pete Haddrell on 10/03/2016.
//
//

#ifndef MainComponent_h
#define MainComponent_h

#include "JuceHeader.h"
#include "VarManager.h"
#include "NeuralNetwork.h"

class MainContentComponent   :  public Component,
                                public Timer
{
public:
    MainContentComponent();
    
    ~MainContentComponent();
    
    void paint (Graphics& g) override;
    
    void resized() override;
    
    void timerCallback() override;
    
    VarManager& getVarManager() { return manager; }
    
private:
    ScopedPointer<Component> pageOne;       //main screen of the application, displayed via a scoped pointer
    VarManager manager;                     //manages the state values of the toggle buttons
    NeuralNetwork neuralNetwork;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};


// (This function is called by the app startup code to create our main component)
Component* createMainContentComponent();//     { return new MainContentComponent(); }

#endif /* MainComponent_h */
