/*
  ==============================================================================

    NeuralNetwork.cpp
    Created: 29 Mar 2016 4:45:51pm
    Author:  Pete Haddrell

  ==============================================================================
*/

#include "NeuralNetwork.h"
#include "MainComponent.h"

NeuralNetwork::NeuralNetwork() :    Thread ("TrainingThread"),
                                    resources (File::getSpecialLocation (File::currentExecutableFile).getParentDirectory())
{
    trainingState = false;
    playback = 0;
    
    startTimer (100);
    
//    resources = File::getSpecialLocation (File::currentExecutableFile).getParentDirectory();
    
    // network setup
    const int numInputs = 4;
    const int numOutputs = 3;
    const int numHiddenNodes = 2;
    
    // create the network object
    network = FloatNeuralNetwork (Ints (numInputs, numHiddenNodes, numOutputs));
    
    // create an array to hold input values
    input = Floats::newClear (numInputs);
    
    // create an array to hold target values
    target = Floats::newClear (numOutputs);
    
    for (int i = 0; i < NETWORKINPUTTOTAL; i++)
    {
        float* input = new float();
        inputArray.add (input);
    }
    
    for (int i = 0; i < NETWORKOUTPUTTOTAL; i++)
    {
        float* output = new float();
        outputArray.add (output);
    }
    startThread();

    fmod.init (resources);
}


NeuralNetwork::~NeuralNetwork()
{
    patterns.clear();
    network.reset();
    stopThread (500);
    stopTimer();
}


void NeuralNetwork::run()
{
    while (! threadShouldExit())
    {
        // thread training runs in a seperate thread
        trainingState = trainingSet();
    }
}


void NeuralNetwork::timerCallback()
{
    const int playbackOld = playback;
    
    playback = owner->getVarManager().getPlayback();
    
    // checks to make sure the neural network has been trained
    if (trainingState)
    {
        // compares the old used playback variable with the new one
        if (playback != playbackOld)
        {
            if (playback == 1)
                fmod.eventStop();
            
            else if (playback == 2)
                fmod.eventPause();
            
            else if (playback == 3)
                fmod.eventStart();
            
        }
    }
}


void NeuralNetwork::update ()
{
    // checks to see if the neural network is still being trained
    if (! trainingState)
    {
        return;
    }
    
    else
    {
        if (owner != nullptr)
        {
            const float latitude = location.getCoordinate().x;
            const float longitude = location.getCoordinate().y;
            
            std::cout << "Real Latitude: " << location.getCoordinate().x <<
            "\nReal Longitude: " << location.getCoordinate().y << "\n";
            
            date.update();
            
            // weather
            if (! owner->getVarManager().getState(0))
                *inputArray.getUnchecked (0) = weather.connect (longitude, latitude);
            else
                *inputArray.getUnchecked (0) = 0.0f;
            
            // time Of Day
            if (! owner->getVarManager().getState(1))
                *inputArray.getUnchecked (1) = date.getTimeOfDayValue();
            else
                *inputArray.getUnchecked (1) = 0.0f;
            
            // time Of Year
            if (! owner->getVarManager().getState(2))
                *inputArray.getUnchecked (2) = date.getTimeOfYearValue();
            else
                *inputArray.getUnchecked (2) = 0.0f;
            
            // crime
            if (! owner->getVarManager().getState (3))
                *inputArray.getUnchecked (3) = crime.connect (longitude, latitude);
            else
                *inputArray.getUnchecked (3) = 0.0f;

            
            propagateNetwork();
            
            if (date.getDayOfYear() == 359)     // christmas day check
                fmod.setEvent (1.0, *outputArray.getUnchecked (1), *outputArray.getUnchecked (2));
            else
                fmod.setEvent (*outputArray.getUnchecked (0),
                               *outputArray.getUnchecked (1),
                               *outputArray.getUnchecked (2));
            
            std::cout << "In1: " << *inputArray.getUnchecked (0)
            << "\nIn2: " << *inputArray.getUnchecked (1)
            << "\nIn3: " << *inputArray.getUnchecked (2)
            << "\nIn4: " << *inputArray.getUnchecked (3)
            << "\n\nOut1: " << *outputArray.getUnchecked (0)
            << "\nOut2: " << *outputArray.getUnchecked (1)
            << "\nOut3: " << *outputArray.getUnchecked (2)
            <<"\n";
        }
    }
}


void NeuralNetwork::propagateNetwork()
{
    // update input array from varaibles
    for (int i = 0; i < inputArray.size(); ++i)
        input.atUnchecked (i) = *inputArray.getUnchecked (i);
    
    // pass through the network
    const Floats& outputs = network.propogate (input);
    
    // update output array from the output of the network
    for (int i = 0; i < outputArray.size(); ++i)
        *outputArray.getUnchecked (i) = outputs.atUnchecked (i);
}


bool NeuralNetwork::trainingSet()
{
    // gets pattern json file from the app bundle
    BinaryFile stream (Text ((const char*) resources.getChildFile ("pattern.json").getFullPathName().toUTF8()));
    
    // the stream of json file is assigned to a json object
    plonk::JSON json (stream);
    
    if (json.isArray())
    {
        const int numPatterns = (int) json.length();

        // the elements of the array is written to the patterns of the neural network
        for (int i = 0; i < numPatterns; ++i)
        {
            const FloatNeuralNetwork::Pattern pattern (json[i]);

            if ((pattern.getInputLength() == input.length()) &&
                (pattern.getTargetLength() == target.length()))
                patterns.add (pattern);
        }
    }

    // the network is trained 2000
    network.train (patterns, 2000);
    
    // signals the thread to exit
    signalThreadShouldExit();
    
    // tells training state that it is done
    return true;
}


void NeuralNetwork::setOwner (MainContentComponent* o)
{
    // ownership of the MainContentComponent is assigned
    owner = o;
}
