/*
  ==============================================================================

    NeuralNetwork.h
    Created: 29 Mar 2016 4:45:51pm
    Author:  Pete Haddrell

  ==============================================================================
*/

#ifndef NEURALNETWORK_H_INCLUDED
#define NEURALNETWORK_H_INCLUDED

#include "JuceHeader.h"
#include "Weather.h"
#include "Date.h"
#include "Location.h"
#include "Crime.h"
#include "VarManager.h"
#include "FmodClient.h"

// macros for the number of nodes in the input and output layers
#define NETWORKINPUTTOTAL 4
#define NETWORKOUTPUTTOTAL 3

class MainContentComponent;

/** A class that handles all neural network functions */
class NeuralNetwork :   Thread,
                        public Timer
{
public:
    /** Constructor */
    NeuralNetwork();
    
    /** Destructor */
    ~NeuralNetwork();
    
    /** Overidden function from the inherited Thread class */
    void run() override;
    
    /** Overidden function from the inherited Timer class */
    void timerCallback() override;
    
    /** Updates the output nodes with the value on the input nodes */
    void update ();
    
    /** Passes the data through the network */
    void propagateNetwork();

    /** Add training sets of data and then trains the network */
    bool trainingSet();
    
    /** Sets the owner so VarManager can be accessed easily*/
    void setOwner (MainContentComponent* o);
    
    
private:
    Component::SafePointer<MainContentComponent> owner;     // allows the ownership of MainContentComponent to be obtained
    
    File resources;                                         // file object to access the file resources
    
    FloatNeuralNetwork network;                             // neural network object
    FloatNeuralNetwork::Patterns patterns;                  // array of training patterns
    
    Floats input;                                           // array of floats to send to the network
    Floats target;                                          // array of floats to use to train the network for a desired target output
    
    OwnedArray<float> inputArray;                           // values of parameters get sent to this
    OwnedArray<float> targetArray;
    OwnedArray<float> outputArray;                          // values from the network get sent to this
    
    Weather weather;                                        // controls values sent to nodes, based on weather information
    Date date;                                              // controls values sent to nodes, based on date information
    Location location;                                      // gets location information from the CoreLocation framework
    Crime crime;                                            // controls values sent to nodes, based on crime information
    
    FmodClient fmod;                                        // manages the fmod project
    
    bool trainingState;                                     // used to track that training status of the neural network
    int playback;                                           // playback value, retrived from VarManager
};


#endif  // NEURALNETWORK_H_INCLUDED
