/*
  ==============================================================================

  This is an automatically generated GUI class created by the Introjucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Introjucer version: 4.1.0

  ------------------------------------------------------------------------------

  The Introjucer is part of the JUCE library - "Jules' Utility Class Extensions"
  Copyright (c) 2015 - ROLI Ltd.

  ==============================================================================
*/

#ifndef __JUCE_HEADER_2F352EB6D2971B6A__
#define __JUCE_HEADER_2F352EB6D2971B6A__

//[Headers]     -- You can add your own extra header files here --
class MainContentComponent;
#include "JuceHeader.h"
//[/Headers]



//==============================================================================
/**
                                                                    //[Comments]
    An auto-generated component, created by the Introjucer.

    Describe your class and how it works here!
                                                                    //[/Comments]
*/
class PageOne  : public Component,
                 public ButtonListener
{
public:
    //==============================================================================
    PageOne ();
    ~PageOne();

    //==============================================================================
    //[UserMethods]     -- You can add your own custom methods in this section.
    /** dismisses the PageTwo overlay */
    void dismissOverlay();

    /** sets the owner of MainContentComponent */
    void setOwner (MainContentComponent* owner);
    //[/UserMethods]

    void paint (Graphics& g) override;
    void resized() override;
    void buttonClicked (Button* buttonThatWasClicked) override;

    // Binary resources:
    static const char* pauseButton_png;
    static const int pauseButton_pngSize;
    static const char* playButton_png;
    static const int playButton_pngSize;
    static const char* background_png;
    static const int background_pngSize;
    static const char* stopButton_png;
    static const int stopButton_pngSize;
    static const char* pauseButtonOn_png;
    static const int pauseButtonOn_pngSize;
    static const char* playButtonOn_png;
    static const int playButtonOn_pngSize;
    static const char* stopButtonOn_png;
    static const int stopButtonOn_pngSize;


private:
    //[UserVariables]   -- You can add your own custom variables in this section.
    void showOverlay();
    Component::SafePointer<MainContentComponent> owner; // is assigned ownership of MainContentComponent
    ComponentAnimator animator;                         // animates PageTwo
    ScopedPointer<Component> overlay;                   // overlays PageTwo
    //[/UserVariables]

    //==============================================================================
    ScopedPointer<TextButton> optionButton;
    ScopedPointer<ImageButton> playButton;
    ScopedPointer<ImageButton> pauseButton;
    ScopedPointer<ImageButton> stopButton;
    Image cachedImage_background_png_1;
    ScopedPointer<Drawable> drawable2;


    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (PageOne)
};

//[EndFile] You can add extra defines here...
//[/EndFile]

#endif   // __JUCE_HEADER_2F352EB6D2971B6A__
