/*
  ==============================================================================

  This is an automatically generated GUI class created by the Introjucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Introjucer version: 4.1.0

  ------------------------------------------------------------------------------

  The Introjucer is part of the JUCE library - "Jules' Utility Class Extensions"
  Copyright (c) 2015 - ROLI Ltd.

  ==============================================================================
*/

#ifndef __JUCE_HEADER_B228052FE3600B62__
#define __JUCE_HEADER_B228052FE3600B62__

//[Headers]     -- You can add your own extra header files here --
#include "JuceHeader.h"
class MainContentComponent;
#include "VarManager.h"
//[/Headers]



//==============================================================================
/**
                                                                    //[Comments]
    An auto-generated component, created by the Introjucer.

    Describe your class and how it works here!
                                                                    //[/Comments]
*/
class PageTwo  : public Component,
                 public ButtonListener
{
public:
    //==============================================================================
    PageTwo ();
    ~PageTwo();

    //==============================================================================
    //[UserMethods]     -- You can add your own custom methods in this section.
    void setOwner (MainContentComponent* owner);

    //[/UserMethods]

    void paint (Graphics& g) override;
    void resized() override;
    void buttonClicked (Button* buttonThatWasClicked) override;

    // Binary resources:
    static const char* menubackground_png;
    static const int menubackground_pngSize;


private:
    //[UserVariables]   -- You can add your own custom variables in this section.
    VarManager manager;
    Component::SafePointer<MainContentComponent> owner;
    //[/UserVariables]

    //==============================================================================
    ScopedPointer<TextButton> backButton;
    ScopedPointer<ToggleButton> weatherToggle;
    ScopedPointer<ToggleButton> crimeToggle;
    ScopedPointer<ToggleButton> timeToggle;
    ScopedPointer<ToggleButton> dateToggle;
    ScopedPointer<Label> label;
    Image cachedImage_menubackground_png_1;


    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (PageTwo)
};

//[EndFile] You can add extra defines here...
//[/EndFile]

#endif   // __JUCE_HEADER_B228052FE3600B62__
