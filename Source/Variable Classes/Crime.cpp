/*
  ==============================================================================

    Crime.cpp
    Created: 3 Feb 2016 8:37:20pm
    Author:  Pete Haddrell

  ==============================================================================
*/

#include "Crime.h"

Crime::Crime() : Thread ("CrimeThread")
{
    startThread();
    connect (0, 0);
}


Crime::~Crime()
{
    stopThread(500);
}


String Crime::createURL(float longitude, float latitude)
{
    String latStr (latitude);
    String longStr (longitude);
    
    // building the url string
    String urlStr = "https://data.police.uk/api/crimes-at-location?date=2015-10&lat=";
    urlStr += latStr;
    urlStr += "&lng=";
    urlStr += longStr;
    
    return urlStr;
}


float Crime::connect (float longit, float latit)
{
    longitude.set (longit);
    latitude.set (latit);
    
    URL url (createURL(longitude.get(), latitude.get()));
    textStream = juce::JSON::toString (url.readEntireTextStream(), true);
    
    // notifies the thread to start
    notify();
    
    return crimeValue.get();
}


float Crime::search (String data)
{
    // search term to look for
    String crimeToSearch = "violent-crime";
    
    char lastStrChar = crimeToSearch.getLastCharacter();
    int searchEndIndex = crimeToSearch.lastIndexOfChar (lastStrChar);
    
    // prevents an invalid number which can cause the while loop to hang
    if (searchEndIndex == -1)
    {
        return 0;
    }
    
    char lastDataChar = data.getLastCharacter();
    int dataEndIndex = data.lastIndexOfChar (lastDataChar);

    int instanceIndex = 0;
    int searchCount = 0;
    
    // runs until the the instanceIndex receives a -1
    do
    {
        instanceIndex = data.indexOfWholeWord (crimeToSearch);
        
        if (instanceIndex == -1)
        {
            return searchCount;
        }
        else
        {
            data = data.substring ((instanceIndex + searchEndIndex), dataEndIndex);
            searchCount++;
        }
    } while (instanceIndex != -1);

    return searchCount;
}


void Crime::run()
{
    while (! threadShouldExit())
    {
        // makes sure the data has been written to the textStream object
        wait (500);
        
        crimeValue.set (search (textStream));
        crimeValue.set (crimeValue.get() * 0.1);
        
        if (crimeValue.get() > 1.0)
            crimeValue.set (1.0);
        else
            crimeValue.set(crimeValue.get());
        
        // pauses the thread until it has been notified
        wait (-1);
    }
}
