/*
  ==============================================================================

    Crime.h
    Created: 3 Feb 2016 8:37:20pm
    Author:  Pete Haddrell

  ==============================================================================
*/

#ifndef CRIME_H_INCLUDED
#define CRIME_H_INCLUDED

#include "JuceHeader.h"
#include "VarManager.h"

/** A class to manage the incomming crime data */
class Crime : Thread
{
public:
    /** Constructor */
    Crime();
    
    /** Destructor */
    ~Crime();
    
    /** Adds variables together to create a custom url string */
    String createURL(float longitude, float latitude);
    
    /** Connects to data.police.uk */
    float connect (float longit, float latit);
    
    /** Searches the incoming data and returns an int of how many instances are found */
    float search (String data);
    
    /** Overriden function from the inherited Thread class */
    void run() override;
    
private:
    Atomic<float> longitude;    // threadsafe longitude
    Atomic<float> latitude;     // threadsafe latitude
    Atomic<float> crimeValue;   // threadsafe crime value
    String textStream;          //  
};



#endif  // CRIME_H_INCLUDED
