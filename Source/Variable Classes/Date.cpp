//
//  Date.cpp
//  13023320-FYP
//
//  Created by Pete Haddrell on 28/01/2016.
//
//

#include "Date.h"

Date::Date()
{
    timeOfDayValue = 0.0;
    timeOfYearValue = 0.0;
}


void Date::update()
{
    time = time.getCurrentTime();
    std::cout << "Time: " << time.toString (true, true) << "\n";
    setTimeOfDayValue (time.getHours());
    setTimeOfYearValue (time.getMonth());
}


void Date::setTimeOfDayValue (int hoursSinceMidnight)
{
    // checks to see if the value is from 12am to 5am
    if (hoursSinceMidnight >= 0 && hoursSinceMidnight < 6)
        timeOfDayValue = (hoursSinceMidnight + 18) * 0.043;
    else
        timeOfDayValue = (hoursSinceMidnight - 6) * 0.043;
}


void Date::setTimeOfYearValue (int month)
{
    // checks to see if the value is from January to Feburary
    if (month >= 0 && month < 2)
        timeOfYearValue = (month + 10) * 0.090;
    else
        timeOfYearValue = (month - 2) * 0.090;
}


float Date::getTimeOfDayValue()
{
    return timeOfDayValue;
}


float Date::getTimeOfYearValue()
{
    return timeOfYearValue;
}


float Date::getDayOfYear()
{
    return time.getDayOfYear();
}
