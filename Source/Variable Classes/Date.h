//
//  Date.hpp
//  13023320-FYP
//
//  Created by Pete Haddrell on 28/01/2016.
//
//

#ifndef Date_h
#define Date_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "VarManager.h"

/** Class to handle the date and time information */
class Date
{
public:
    /** Constructor */
    Date();
    
    /** Destructor */
    ~Date()     {};
    
    /** Updates the time of day */
    void update();
    
    /** Mutator to change the value of the time of day */
    void setTimeOfDayValue (int hoursSinceMidnight);
    
    /** Mutator to change the value of the time of year */
    void setTimeOfYearValue (int month);
    
    /** Accessor to return the value of the time of day */
    float getTimeOfDayValue();
    
    /** Acessor to return the value of the time of year */
    float getTimeOfYearValue();
    
    /** Returns the day number of the year */
    float getDayOfYear();
    
private:
    Time time;              // allows retrieval of Time information
    float timeOfDayValue;   // stores the value of the time of day
    float timeOfYearValue;  // store the value of the time of year
};

#endif /* Date_h */
