//
//  Location.m
//  13023320-FYP
//
//  Created by Pete Haddrell on 03/02/2016.
//
//

#include "Location.h"

// Objective-C frameworks
#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>


/** A class that uses Objective-C to use CoreLocation */
class LocationPeer
{
public:
    /** Constructor */
    LocationPeer()
    {
        // setup for CoreLocation
        locationManager = [[CLLocationManager alloc] init];
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
        
        // does a permission check to see if the user has allowed the app to use location data
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        {
            [locationManager requestWhenInUseAuthorization];
        }
        
        [locationManager startUpdatingLocation];
    }
    
    CLLocationManager *locationManager;
};


Location::Location()
:   peer (new LocationPeer())
{}

Location::~Location()
{
    
}

Point<double> Location::getCoordinate()
{
    CLLocationCoordinate2D coordinate = peer->locationManager.location.coordinate;
    return Point<double> (coordinate.latitude, coordinate.longitude);
}
