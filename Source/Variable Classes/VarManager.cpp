/*
  ==============================================================================

    VarManager.cpp
    Created: 10 Feb 2016 4:56:58pm
    Author:  Pete Haddrell

  ==============================================================================
*/

#include "VarManager.h"

bool VarManager::states[4];
int VarManager::playback;

VarManager::VarManager()
{}

void VarManager::setState (int varID, bool state)
{
    states[varID] = state;
}

bool VarManager::getState (int varID)
{
    return states[varID];
}

void VarManager::setPlayback (int mode)
{
    playback = mode;
}

int VarManager::getPlayback()
{
    return playback;
}
