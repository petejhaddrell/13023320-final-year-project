/*
  ==============================================================================

    VarManager.h
    Created: 10 Feb 2016 4:56:58pm
    Author:  Pete Haddrell

  ==============================================================================
*/

#ifndef VARMANAGER_H_INCLUDED
#define VARMANAGER_H_INCLUDED

#include "JuceHeader.h"

/** A class to handle the on/off state of considered variables and playback of the music */
class VarManager
{
public:
    /** Constructor */
    VarManager();
    
    /** Destructor */
    ~VarManager()   {};
    
    /** Mutator to set the state of a variable at a defined element */
    void setState (int varID, bool state);
    
    /** Accessor that returns the variable state of a requested element */
    bool getState (int varID);
    
    /** Mutator to set the value of playback  */
    void setPlayback (int mode);
    
    /** Accessor that returns the playback value */
    int getPlayback();
    
private:
    static bool states[4];  // an array of states for each variable
    static int playback;    // the playback value
};

#endif  // VARMANAGER_H_INCLUDED
