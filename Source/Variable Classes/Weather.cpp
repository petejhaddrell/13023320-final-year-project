//
//  Weather.cpp
//  13023320-FYP
//
//  Created by Pete Haddrell on 24/03/2016.
//
//

#include "Weather.h"

Weather::Weather() :    Thread ("WeatherThread"),
                        apiKey ("00696163f0f5c530cea048d873b56179")
{
    weatherVal = 0.0;
    startThread();
    connect (0, 0);
}


Weather::~Weather()
{
    apiKey.clear();
    stopThread (500);
}


String Weather::createURL (float longitude, float latitude)
{
    String longStr (longitude);
    String latStr (latitude);
    
    //building the url string
    String urlStr = "https://api.forecast.io/forecast/";
    urlStr += apiKey;
    urlStr += "/";
    urlStr += longStr;
    urlStr += ",";
    urlStr += latStr;
    
    return urlStr;
}


float Weather::connect (float longit, float latit)
{
    latitude.set (latit);
    longitude.set (longit);
    
    //notifies the thread to start running
    notify();
    
    return weatherVal.get();
}


void Weather::setWeatherValue (String const& weather)
{
    std::cout << "Weather String: " << weather << "\n";
    
    if (weather.contains ("clear"))
        weatherVal = 0.1f;
    else if (weather.contains ("wind"))
        weatherVal = 0.2f;
    else if (weather.contains ("partly-cloudy"))
        weatherVal = 0.3f;
    else if (weather.contains ("cloudy"))
        weatherVal = 0.4f;
    else if (weather.contains ("fog"))
        weatherVal = 0.5f;
    else if (weather.contains ("hail"))
        weatherVal = 0.6f;
    else if (weather.contains ("sleet"))
        weatherVal = 0.7f;
    else if (weather.contains ("rain"))
        weatherVal = 0.8f;
    else if (weather.contains ("snow"))
        weatherVal = 0.9f;
    else if (weather.contains ("thunderstorm"))
        weatherVal = 1.0f;
    else
        weatherVal = 0.0f;              //default
}


void Weather::run()
{
    while (! threadShouldExit())
    {
        // makes the thread wait to ensure data has been written to the variables
        wait (500);
        
        URL url (createURL(longitude.get(), latitude.get()));
        String textStream = juce::JSON::toString (url.readEntireTextStream(), true);
        int summaryIndex = textStream.indexOfWholeWord("icon");
        
        // shortens the string to isolate usable data
        setWeatherValue (textStream.substring (summaryIndex, summaryIndex + 30));
        
        // makes the thread wait until it has been notified to run
        wait (-1);
    }
}
