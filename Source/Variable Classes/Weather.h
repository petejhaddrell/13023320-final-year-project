//
//  Weather.hpp
//  13023320-FYP
//
//  Created by Pete Haddrell on 24/03/2016.
//
//

#ifndef Weather_h
#define Weather_h

#include "JuceHeader.h"
#include "VarManager.h"

/** A class that uses the Dark Sky servers to retrieve weather information */
class Weather : Thread
{
public:
    /** Constructor */
    Weather();
    
    /** Destructor */
    ~Weather();
    
    /** Adds variables together to create a custom url string */
    String createURL(float longitude, float latitude);
    
    /** Connects to dark sky */
    float connect (float longitude, float latitude);
    
    /** Mutator to take the weather data and assign a value */
    void setWeatherValue (String const& weather);
    
    /** Overridden function from the inherrited Thread class */
    void run() override;
    
private:
    String apiKey;              // api key kept within a string object
    Atomic<float> weatherVal;   // threadsafe weather value
    Atomic<float> longitude;    // threadsafe longitude
    Atomic<float> latitude;     // threadsafe latitude
};

#endif /* Weather_h */
